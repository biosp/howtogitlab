# l'ABC de GitLab

__Bienvenue sur le GitLab de BioSP !__

## Présentation

Depuis son utilisation en 2014, __GitLab__ a énormément évolué et propose dorénavant beaucoup de fonctionnalités. il peut en rebuter quelques uns au premier abord mais il reste un outil dont la prise en main est rapide.  
__Gitlab__ est une plateforme basé sur __GIT__ pour le développement et la gestion de projets informatiques. Pour avoir une vue d'ensemble le mieux c'est d'aller voir directement sur [la page de gitlab](https://gitlab.com) ou dans la [doc](https://docs.gitlab.com/ee/README.html).

### Mais c'est quoi __Git__?

__Git__ est un logiciel de gestion de versions. Il permet de maintenir l'ensemble des versions d'un ou plusieurs fichiers.

En gros, __GitLab__ est une interface qui permet d'utiliser __Git__, plus plein de petits outils/fonctionnalités très pratiques.

### Encore un truc d'informaticien où j'y comprends rien, c'est pas pour moi ça !

Pas si sûr !

__GitLab__ va vous permettre de versionner votre __code__, mais aussi vos __publications__, __documentations__ et __résultats__.  
Il va vous permettre de travailler à plusieurs sur le même projet, d'avoir différentes versions du même projet, d'automatiser beaucoup de choses (test, packaging, deploiement...), de reproduire des expériences à l'identique, de partager facilement votre projet avec vos communautés et collègues, etc.  

### Pourquoi GitLab et pas autre chose ?

Pourquoi refuser d'utiliser un outil moderne qui est complet, facile d'utilisation, répondant à nos problématiques et qui est proposé dans une version opensource et libre.

## Pour aller plus loin

__Pour accéder à la suite c'est par [ICI](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/home "Wiki l'ABC de git") !__

__Un problème technique ?__ : <gitlab-biosp-admin@inrae.fr> (l'équipe GitLab Paca)

__Pour avoir de l'aide__ : <gitlab-biosp-users@inrae.fr> (liste de discussion et d'information des utilisateurs GitLab Paca)  

## Accés direct

[Page principale](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/home)

1. [Présentation](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/presentation)
2. [S'inscrire](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/inscription)
3. [Créer un projet](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/projet)
4. [Configurer mon ordinateur](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/configurationordinateur) 
5. [Premiers pas](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/debut)
6. [Commandes de base](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/Commandes)
7. [CI/CD](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/CI)
8. [Container registry](https://gitlab.paca.inrae.fr/biosp/howtogitlab/wikis/registry)

## Recettes (CookBooks)

* [écosystème R](https://gitlab.paca.inrae.fr/r-ecosystem/cookbooks) : Packaging R, R Shiny App...

## Présentation Git/GitLab

* (2019) [Poster useR2019 - GitLab CI/CD and R packages](https://gitlab.paca.inrae.fr/biosp/howtogitlab/blob/master/presentation/poster_JF_REY_R_GitLab.pdf)

* (2018) [Présentation Git et GitLab PACA](https://gitlab.paca.inrae.fr/biosp/howtogitlab/blob/master/presentation/Git_GitLab_BioSP_INRA_PACA.pdf)

* (2014) [PresentationGit.pdf](https://gitlab.paca.inrae.fr/biosp/howtogitlab/blob/master/presentation/PresentationGit.pdf)
> La partie GitLab n'est plus à jour dans la présentation

